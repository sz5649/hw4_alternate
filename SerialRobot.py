from RigidBody import *

#Robot is defined by a list of joints, each with a parent joint and chilc link
#The joint list should be passed in at initialization. 
#General TODO: clean up transforms by executing SE2 multiplications by hand 
class SerialRobot:
	def __init__(self, name_, joints_=[], grav_=np.zeros(2)):
		self.name = name_
		self.joints = joints_
		self.ndofs = len(joints_)
		self.q = np.zeros(self.ndofs)
		self.qdot = np.zeros(self.ndofs)
		self.command = np.zeros(self.ndofs)
		self.grav = grav_
		self.transforms = []
		self.jointNames = []
		if(len(joints_))>0:
			self.__setup()
		else:
			print "WARNING: BLANK ROBOT"

	#setup sorts the joint list so they are in the correct order
	def __setup(self):
		jtemp = self.joints[:]
		#sorts the list of joints. 
		#if multiple joints have the same index, this overwrites
		for ii in range(0,len(jtemp)):
			jtemp[self.joints[ii].getIdx()] = self.joints[ii] 
		
		#setup other lists
		#list of joint names sorted by joint index
		#list of recursive transforms for each frame mapping (point in joint frame)-->(point in inertial reference)
		self.jointNames.append(jtemp[0].getName())
		self.transforms.append(jtemp[0].getTransformAsMatrix())
		for jj in range(1,len(jtemp)):
			self.jointNames.append(jtemp[jj].getName())	
			Tjj_prev = self.transforms[jj-1]
			self.transforms.append(np.dot(Tjj_prev,jtemp[jj].getTransformAsMatrix()))	

		self.joints = jtemp

	def getJacobian(self,jointIdx_,point_=np.zeros(2)):
		#initialize Jacobian of size 3xndofs to zero
		J = np.zeros((3,self.ndofs))
		#calculate the point inthe inertial reference frame
		T = self.transforms[jointIdx_]
		p_inertial = np.dot(T[0:2,0:2],point_) + T[0:2,2]
		#set each column of the Jacobian accounting for joint type
		for jj in range(0,jointIdx_+1):
			Tjj = self.transforms[jj]
			if self.joints[jj].getAxIdx() == 0: #revolute joint
				p_jj_inertial = Tjj[0:2,2]
				p_arm = p_inertial - p_jj_inertial
				J[0,jj] = 1.0
				J[1,jj] = p_arm[1]
				J[2,jj] = -p_arm[0]
			else: #prismatic joint
				axis_joint = np.zeros((2,1))
				axis_joint[self.joints[jj].getAxIdx()-1] = 1.0
				axis_inertial = np.dot(Tjj[0:2,0:2],axis_joint)
				J[1,jj] = axis_inertial[0]
				J[2,jj] = axis_inertial[1] 
		return J

	def getMassMatrix(self):
		M = np.zeros((self.ndofs,self.ndofs))
		spatial_inertia_jj = np.zeros((3,3))
		for jj in range(0,self.ndofs):
			com_jj = self.joints[jj].link.inertia.getCOM()
			m_jj = self.joints[jj].link.inertia.getM()
			i_jj = self.joints[jj].link.inertia.getI()
			Jjj_com = self.getJacobian(jj,com_jj)
			spatial_inertia_jj[0,0] = i_jj
			spatial_inertia_jj[1,1] = m_jj
			spatial_inertia_jj[2,2] = m_jj
			M += np.dot(np.dot(np.transpose(Jjj_com),spatial_inertia_jj),Jjj_com)

		return M

	def getGravity(self):
		g = np.zeros(self.ndofs)
		for jj in range(0,self.ndofs):
			com_jj = self.joints[jj].link.inertia.getCOM()
			m_jj = self.joints[jj].link.inertia.getM()
			Jjj_com = self.getJacobian(jj,com_jj)
			Jjj_com_v = Jjj_com[1:3,:]
			g -= m_jj*np.dot(np.transpose(Jjj_com_v),self.grav)

		return g

	def getPoint(self,jointIdx_,point_=np.zeros((2,1))):
		T = self.transforms[jointIdx_]
		p_inertial = np.dot(T[0:2,0:2],point_) + T[0:2,2]
		return p_inertial

	def getTransformAsMatrix(self,jointIdx_):
		T = self.transforms[jointIdx_]
		return T

	def setPosition(self,q_):
		self.q = q_

	def setVelocity(self,qdot_):
		self.qdot = qdot_

	def setState(self,q_,qdot_):
		self.q = q_
		self.qdot = qdot_

	def setCommand(self, command_):
		startIdx = self.ndofs-len(command_)
		self.command[startIdx:len(command_)] = command_

	def getJoint(self,jointIdx_):
		return self.joints[jointIdx_]

	def getLink(self,jointIdx_):
		return self.joints[jointIdx_].link

	def update(self):
		#Update individual joints
		#Store successive transforms back to inertial frame
		for jj in range(0,len(self.joints)):
			self.joints[jj].update(self.q[jj])
			if jj>0:
				Tjj_prev = self.transforms[jj-1]
				self.transforms[jj] = np.dot(Tjj_prev,self.joints[jj].getTransformAsMatrix())
			else:
				self.transforms[0] = self.joints[0].getTransformAsMatrix()

	def draw(self,ax_,colColor_='r',visColor_='g'):
		for jj in range(0,len(self.joints)):
			link_jj = self.getLink(jj)
			for kk in range(0,len(link_jj.visuals)):
				link_jj.visuals[kk].updateFromMatrix(self.transforms[jj])
				link_jj.visuals[kk].draw(ax_,visColor_)
			for ll in range(0,len(link_jj.collisions)):
				link_jj.collisions[ll].updateFromMatrix(self.transforms[jj])
				link_jj.collisions[ll].draw(ax_,colColor_)