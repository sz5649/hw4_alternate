from SerialRobot import *
from RobotForces import *
from RobotSimulation import *

#first three joints are the virtual joints so all relevant information
#about the "base" body is set for the child link of the 3rd joint (here, L2)
#x-direction virtual joint
I0=Inertia("I0",0,0,np.zeros(2))
L0=Link("link0",I0)
T0=Transform()
J0=Joint("joint0_virtual",1,-1, T0, L0)

#y-direction virtual joint
I1=Inertia("I1",0,0,np.zeros(2))
L1=Link("link1",I1)
T1=Transform(np.zeros(2),0.0)
J1=Joint("joint1_virtual",2, 0, T1, L1)

#theta-direction virtual joint
#Inertia for floating base
I2=Inertia("I2",10,1,np.zeros(2))
L2=Link("link2_base",I2)
#Visual and collision geometries
S2_vis=[Rectangle("link2_rect_vis",0.2,0.2)]
T2_corner1 = Transform(0.09*np.array([-np.cos(np.pi/4),-np.sin(np.pi/4)]),0.0)
T2_corner2 = Transform(0.09*np.array([np.cos(np.pi/4),-np.sin(np.pi/4)]),0.0)
T2_corner3 = Transform(0.09*np.array([-np.cos(np.pi/4),np.sin(np.pi/4)]),0.0)
T2_corner4 = Transform(0.09*np.array([np.cos(np.pi/4),np.sin(np.pi/4)]),0.0)
S2_collision=[Circle("c1",0.04,T2_corner1),
			  Circle("c2",0.04,T2_corner2),
			  Circle("c2",0.04,T2_corner3),
			  Circle("c2",0.04,T2_corner4)]
L2.setVisuals(S2_vis)
L2.setCollisions(S2_collision)
T2=Transform(np.zeros(2),0.0)
J2=Joint("joint2_virtual",0, 1, T2, L2)

#We also attach a link with one real joint
#Inertial properties of the second real link
I3=Inertia("I3",2,1,np.array([0.1,0]))
L3=Link("link3_real",I3)
#Visual and collision geometries
S3_vis=[Line("link3_line_vis",np.array([0,0]),np.array([0,0.2]))]
T3_tip=Transform(np.array([0,0.2]),0.0)
S3_collision=[Circle("c3",0.01,T3_tip)]
L3.setVisuals(S3_vis)
L3.setCollisions(S3_collision)
T3=Transform(np.array([0.0,0.1]),0)
J3=Joint("joint3_real",0,2,T3,L3)

#Build the robot
floatingJoints = [J1,J0,J2,J3]
floatingRobot = SerialRobot("floatingRobot",floatingJoints,np.array([0,-9.8]))

p0 = np.array([1,1])
q0 = np.array([0,0.1,0,0])
print "setting joint positions to:"
print q0
floatingRobot.setPosition(q0)
floatingRobot.update()
Jfloat = floatingRobot.getJacobian(3,p0)
print "Jacobian:"
print Jfloat
gfloat = floatingRobot.getGravity()
print "gravity:"
print gfloat
Mfloat = floatingRobot.getMassMatrix()
print "mass:"
print Mfloat

#Set up forces on the robot
floorNormal = np.array([0,1])
floorPoint = np.zeros(2)
rootBodyIndex = 2
B3_contact = ExternalContact(floorPoint,floorNormal,100000,1000,rootBodyIndex,floatingRobot)
EE_contact = ExternalContact(floorPoint,floorNormal,100000,1000,rootBodyIndex+1,floatingRobot)

muscleAnchor_root = np.array([-0.05,0.05])
muscleAnchor_EE_link = np.array([0,0.05])
M1_muscle = LinearMuscle(20000,rootBodyIndex,muscleAnchor_root,
						   rootBodyIndex+1,muscleAnchor_EE_link,0.05,floatingRobot)

print type(S3_vis[0])
forces = [B3_contact,EE_contact,M1_muscle]
sim = RobotSimulation(0.005,floatingRobot,forces)
qdot0 = np.zeros(4)
sim.initialize(q0,qdot0)
sim.step(200)
sim.animate()

# floatingRobot.setPosition(q0)
# floatingRobot.update()

# f = plt.figure()
# f.gca().set_xlim(-.5,.5)
# f.gca().set_ylim(-0.2,0.5)
# floatingRobot.draw(f.gca())
# # S3_collision[0].draw(f.gca())
# # S2_vis[0].draw(f.gca())
# # S3_vis[0].draw(f.gca())
# plt.show()
