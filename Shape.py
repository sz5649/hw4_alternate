import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as pth
import matplotlib.patches as patches

from Transform import *
#Base class for visual and collision shapes
#All shapes are defined w.r.t. a link (parent joint frame), but can be transformed within that frame
#All shapes should be able to draw themselves in their parent frame
class Shape:
	def __init__(self,name_,transform_=Transform(np.zeros(2),0.0)):
		self.name = name_
		self.T0 = transform_

	def getStaticTransform(self):
		return self.T0

	def update(self, baseTransform_=Transform(np.zeros(2),0.0)):
		#updates internal variables in preparation for drawing a figure
		print "UPDATE NOT IMPLEMENTED"
		pass

	def updateFromMatrix(self, baseTMatrix_):
		#updates internal variables in preparation for drawing a figure
		print "UPDATE FROM MATRIX NOT IMPLEMENTED"
		pass

	def draw(self, ax_):
		#draws the object
		print "DRAW NOT IMPLEMENTED"
		pass

#Line from p1 to p2
class Line(Shape):
	def __init__(self,name_,p1_,p2_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)
		self.p1 = p1_
		self.p2 = p2_
		self.v0 = [p1_,p2_]
		self.vertices = [p1_,p2_]
		self.update()

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		T0 = np.dot(baseTransform_.getInverseSE2(),self.T0.getInverseSE2())
		self.updateFromMatrix(T0)

	def updateFromMatrix(self, baseTMatrix_):
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		self.vertices[0] = np.dot(T0[0:2,0:2],self.p1) + T0[0:2,2]
		self.vertices[1] = np.dot(T0[0:2,0:2],self.p2) + T0[0:2,2]

	def draw(self, ax_, color_='r',lw_=2):
		vtemp = self.vertices[:]
		codes = [pth.Path.MOVETO,pth.Path.LINETO]
		path = pth.Path(vtemp,codes)
		patch = patches.PathPatch(path,facecolor=color_,lw=lw_)
		ax_.add_patch(patch)

#Rectangle centered at (0,0) with width in x-direction and height in y-direction
class Rectangle(Shape):
	def __init__(self,name_,width_,height_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)
		self.width = width_
		self.height = height_
		self.v0 = [np.array([width_/2.0,height_/2.0]),
						 np.array([-width_/2.0,height_/2.0]),
						 np.array([-width_/2.0,-height_/2.0]),
						 np.array([width_/2.0,-height_/2.0])]
		self.vertices = [np.array([width_/2.0,height_/2.0]),
						 np.array([-width_/2.0,height_/2.0]),
						 np.array([-width_/2.0,-height_/2.0]),
						 np.array([width_/2.0,-height_/2.0])]
		self.update()

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		T0 = np.dot(baseTransform_.getInverseSE2(),self.T0.getInverseSE2())
		self.updateFromMatrix(T0)

	def updateFromMatrix(self, baseTMatrix_):
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		for ii in range(0,len(self.vertices)):
			self.vertices[ii] = np.dot(T0[0:2,0:2],self.v0[ii]) + T0[0:2,2]

	def draw(self, ax_, color_='r',lw_=2):
		vtemp = self.vertices[:]
		vtemp.append(np.zeros(2))
		codes = [pth.Path.MOVETO,pth.Path.LINETO,pth.Path.LINETO,pth.Path.LINETO,pth.Path.CLOSEPOLY]
		path = pth.Path(vtemp,codes)
		patch = patches.PathPatch(path,facecolor=color_,lw=lw_)
		ax_.add_patch(patch)

#Circle of radius radius_ centered at (0,0)
class Circle(Shape):
	def __init__(self,name_,radius_,transform_=Transform(np.zeros(2),0.0)):
		Shape.__init__(self,name_,transform_)
		self.radius = radius_
		self.center = np.zeros(2)
		self.update()

	def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
		T0 = baseTransform_.getInverseSE2(),self.T0.getInverseSE2()
		self.updateFromMatrix(T0)

	def updateFromMatrix(self, baseTMatrix_):
		T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
		self.center = T0[0:2,2]		

	def draw(self,ax_,color_='r'):
		circ = plt.Circle((self.center[0],self.center[1]),radius=self.radius,color=color_)
		ax_.add_patch(circ)

