from RobotSimulation import *
# setup the 3-link floating robot
import RobotSetup as rs

# initial conditions
q0 = np.array([0.0,0.051,0.0,-np.pi/12,np.pi/6,-np.pi/12])
qdot0 = np.zeros(6)

# instantiate and run the simulation
sim = RobotSimulation(0.001,rs.legRobot,rs.legRobotForces)
sim.initialize(q0,qdot0)
sim.step(500)
sim.animate()

