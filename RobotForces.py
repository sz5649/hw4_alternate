from SerialRobot import *
from math import sqrt, fabs

#base class to encapsulate forces (note: explicitly NOT wrenches)
#might be nice to make this an abstract base class
class RobotForce:
	def __init__(self, name_):
		self.name = name_
		self.F = []
		self.J = []

	def getForce(self):
		return self.F

	def getJacobian(self):
		return self.J

	def update(self,robot_=SerialRobot("default")):
		print "NOT IMPLEMENTED"
		pass

#external contact forces via penalty method with damping
#for now this is explicit for circles and half-planes
#BONUS: extend to line/rectangle vertices
class ExternalContact(RobotForce):
	def __init__(self, name_, planePoint_, planeNormal_, K_, D_, jointIdx_,
				 robot_=SerialRobot("default")):
		RobotForce.__init__(self,name_)
		self.K = K_
		self.D = D_
		self.point = planePoint_
		self.normal = planeNormal_
		self.idx = jointIdx_
		if robot_.ndofs>0:
			self.__setup(robot_)
			self.update(robot_)
		else:
			"WARNING: CONTACT FORCE ON BLANK ROBOT"

	#setup containers for forces and associated Jacobians
	#TODO: check that contact shapes are all circles OR extend to vertices
	def __setup(self,robot_=SerialRobot("default")):
		collisions = robot_.getLink(self.idx).collisions
		for ii in range(0,len(collisions)):
			self.F.append(np.zeros(2))
			self.J.append(np.zeros((2,robot_.ndofs)))

	def update(self,robot_=SerialRobot("default")):
		collisions = robot_.getLink(self.idx).collisions
		T = robot_.getTransformAsMatrix(self.idx)
		for ii in range(0,len(collisions)):
			collisions[ii].updateFromMatrix(T)
			point_to_center = np.array([collisions[ii].center[0]-self.point[0],
						   				collisions[ii].center[1]-self.point[1]])
			signed_distance = np.dot(point_to_center,self.normal)
			if(signed_distance <= 0): #contact has occured, F = K*N*|d| - D*vel
				center_joint = collisions[ii].T0.getInverseSE2()[0:2,2]
				self.J[ii] = robot_.getJacobian(self.idx,center_joint)[1:3,:]
				v_point = np.dot(self.J[ii],robot_.qdot)
				self.F[ii] = self.K * fabs(signed_distance) * self.normal - self.D * v_point
			else: #no contact
				self.F[ii] = np.zeros(2)
				self.J[ii] = np.zeros((2,robot_.ndofs))
			
#elastic muscles 
class LinearMuscle(RobotForce):
	def __init__(self,name_,K_,joint1Idx_,point1_,joint2Idx_,point2_,preloadDelta_,
				 robot_=SerialRobot("default")):
		RobotForce.__init__(self,name_)
		self.K = K_
		self.idx1 = joint1Idx_
		self.p1 = point1_
		self.idx2 = joint2Idx_
		self.p2 = point2_
		self.L0 = 0
		self.mag = 0
		self.dir = np.zeros(2)
		if robot_.ndofs>0:
			self.__setup(preloadDelta_,robot_)
			self.update(robot_)
		else:
			"WARNING: MUSCLE FORCE ON BLANK ROBOT"

	def __setup(self,preloadDelta_,robot_=SerialRobot("default")):
		for ii in range(0,2):
			self.F.append(np.zeros(2))
			self.J.append(np.zeros((2,robot_.ndofs)))
		p1_inertial = robot_.getPoint(self.idx1,self.p1)
		p2_inertial = robot_.getPoint(self.idx2,self.p2)
		d0 = p2_inertial - p1_inertial
		L0 = sqrt(d0[0]**2 + d0[1]**2)
		self.L0 = fabs(L0-preloadDelta_)

	def update(self,robot_):
		p1_inertial = robot_.getPoint(self.idx1,self.p1)
		p2_inertial = robot_.getPoint(self.idx2,self.p2)
		d = p2_inertial - p1_inertial
		L = sqrt(d[0]**2 + d[1]**2)
		self.mag = self.K*(L - self.L0)
		if fabs(L) > 1e-5:
			self.dir = d/L

		self.F[0] = -self.mag * self.dir
		self.F[1] = -self.F[0]

		self.J[0] = robot_.getJacobian(self.idx1,self.p1)[1:3,:]
		self.J[1] = robot_.getJacobian(self.idx2,self.p2)[1:3,:]
