import numpy as np
from Transform import *
from Shape import *

#Base class for inertial parameters of rigid bodies
#I is defined about center of mass unless otherwise noted
class Inertia:
	def __init__(self,name_,M_,I_,COM_):
		self.name = name_
		self.M = 0.0
		self.I = 0.0
		self.COM = np.zeros(2) 
		self.setParameters(M_,I_,COM_)

	def setParameters(self,M_,I_,COM_):
		self.M = M_
		self.I = I_
		self.COM = COM_

	def setName(self,name_):
		self.name = name_

	def getCOM(self):
		return self.COM

	def getM(self):
		return self.M

	def getI(self):
		return self.I


#Base class for robot links.  Links contain all information about
#the inertial parameters, visualization, and collision shames of the body
#The reference frame is defined by parent (joint) index
#There is a 1:1 relationship between joints and links
class Link:
	def __init__(self,name_,inertia_=Inertia("default",0.0,0.0,np.zeros((2,1)))):
		self.name = name_
		self.collisions = []
		self.visuals = []
		#This needs to be defined w.r.t. the parent joint frame, NOT the center of mass
		#However, the inertia passed in represents the inertia w.r.t. the COM
		Iname = name_+"_inertia"
		inertia_.setName(Iname)
		self.inertia = inertia_
		self.__setInertia(inertia_)

	#inertia_ should be the net inertia w.r.t. the center of mass of the link
	#BONUS: accept a list of inertias, calculate net inertia.
	def __setInertia(self,inertia_):
		#Making use of parallel axis theorem so rotational inertia is defined w.r.t.the joint frame
		d2 = inertia_.COM[0]**2 + inertia_.COM[1]**2
		self.inertia.setParameters(inertia_.M,inertia_.I + d2*inertia_.M,inertia_.COM)

	def setVisuals(self,visuals_=[]):
		self.visuals = visuals_

	def setCollisions(self,collisions_=[]):
		self.collisions = collisions_

#A joint defined by parent (-1 for the first joint), 
#the transform from that parent,
#the link properties,
#and an axis index (0=theta, 1=x, 2=y)
class Joint:
	def __init__(self, name_, axisIdx_=0, parentIdx_=-1, transform_=Transform(np.zeros(2),0.0), link_=Link("default")):
		self.name = name_
		self.parentIdx = parentIdx_
		self.axisIdx = axisIdx_
		self.T0 = transform_
		self.TJ = Transform(np.zeros(2),0.0)
		self.link = link_

	def setLink(self, link_):
		self.link = link_

	def getIdx(self):
		return self.parentIdx+1

	def getAxIdx(self):
		return self.axisIdx

	def getName(self):
		return self.name

	def update(self,q):
		if self.axisIdx == 0: #revolute joint
			self.TJ.updateTheta(q)
		else: #prismatic joint
			d = np.zeros(2)
			d[self.axisIdx-1] = q
			self.TJ.updateDisplacement(d)

	def getTransformAsMatrix(self):
		return  np.dot(self.T0.getInverseSE2(),self.TJ.getInverseSE2())